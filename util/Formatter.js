jQuery.sap.declare("com.springer.fifsresponsefeed.util.Formatter");
jQuery.sap.require("com.springer.fifsresponsefeed.util.DateHandler");
jQuery.sap.require("sap.ui.core.format.NumberFormat");

com.springer.fifsresponsefeed.util.Formatter = {

	getDateSimple: function(value) {
		if (!value || typeof value === "undefined") {
			return null;
		}
		if (!(value instanceof Date) && value.indexOf("Date")) {
			var date = new Date(parseInt(value.substr(6))).format('dd.mm.yyyy HH:mm');
		} else {
			var date = new Date(value).format('dd.mm.yyyy HH:mm');
		}
		return date;
	},
	getMessageTypeIcon: function(value) {
		switch (value) {
			case "E":
				return "sap-icon://message-error";
			case "W":
				return "sap-icon://message-warning";
			case "S":
				return "sap-icon://message-success";
			case "U":
				return "sap-icon://toaster-top";
			default:
				return "sap-icon://message-information";
		}
	},
	getMSGSourceInfo: function(vOrigin, vDepartment, vCategory) {
		var vDepCatText = "";
		if(vDepartment !== "" || vCategory !== "") {
			vDepCatText = vDepartment + " / " + com.springer.fifsresponsefeed.util.Formatter.getCaegoryTXT( vCategory );
		}
		
		var vReturnValue = "Source: " + vOrigin;
		if ( vDepCatText !== "" ) {
			vReturnValue = vReturnValue + " / " + vDepCatText;
		}
		return vReturnValue;
		
	},
	getCaegoryTXT: function(vCategory) {
		if (typeof this.oCategoryModel === "undefined") {
			this.oCategoryModel = sap.ui.getCore().getModel("CategoryModel");
		}
		if (typeof this.oCategoryModel !== "undefined") {
			var categoryDetails = this.oCategoryModel.getProperty("/CategoryDetails");
			for (var i = 0; i < categoryDetails.length; i++) {
				if (vCategory === categoryDetails[i].Category) {
					return categoryDetails[i].CategoryTXT;
				}
			}
		}
		return vCategory;
	},
	overlayTenZero: function(value) {
		value = value + "";
		while (value.length < 10) {
			value = "0" + value;
		}
		return value;
	}

};