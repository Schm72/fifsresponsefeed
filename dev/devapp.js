jQuery.sap.declare("com.springer.fifsresponsefeed.dev.devapp");
jQuery.sap.require("com.springer.fifsresponsefeed.dev.devlogon");

com.springer.fifsresponsefeed.dev.devapp = {
	smpInfo: {},
	entityName: null,
	navName: null,
	isLoaded: false,
	externalURL: null,
	isOnline: false,
	
	//Application Constructor
	initialize: function() {
		this.entityName = "AppMessageFeedSet";
		this.bindEvents();
	},

	//========================================================================
	// Bind Event Listeners
	//========================================================================
	bindEvents: function() {
		//add an event listener for the Cordova deviceReady event.
		document.addEventListener("deviceready", this.onDeviceReady, false);
		//document.addEventListener("online", this.deviceOnline, false);
		//document.addEventListener("offline", this.deviceOffline, false);
		//document.addEventListener("pause", this.onPause, false);
		//document.addEventListener("resume", this.onResume, false);
	},
	
	//========================================================================
	//Cordova Device Ready
	//========================================================================
	onDeviceReady: function() {
		console.log("onDeviceReady");
		startApp();
	}

};