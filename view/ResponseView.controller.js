jQuery.sap.require("com.springer.fifsresponsefeed.util.Formatter");
jQuery.sap.require("com.springer.fifsresponsefeed.util.Controller");

com.springer.fifsresponsefeed.util.Controller.extend("com.springer.fifsresponsefeed.view.ResponseView", {

	onInit: function() {
		this.getView().setBusy(true);
		console.log("Init Feed List");
		this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);

		// building a custom JSON model
		this.oMessageTypeModel = new sap.ui.model.json.JSONModel();
		this.oMessageTypeModel.setData({
			"MessageTypeDetails": [{
				"MessageType": "I",
				"MessageTypeTXT": "Information"
			}, {
				"MessageType": "S",
				"MessageTypeTXT": "Success"
			}, {
				"MessageType": "W",
				"MessageTypeTXT": "Warning"
			}, {
				"MessageType": "E",
				"MessageTypeTXT": "Error"
			}]
		});
		sap.ui.getCore().setModel(this.oMessageTypeModel, "MessageTypeModel");

		// build category code to Text model
		this.oCategoryModel = new sap.ui.model.json.JSONModel();
		this.oCategoryModel.setData({
			"CategoryDetails": [
				{
					"Category": "QS",
					"CategoryTXT": "Question"
				}, {
					"Category": "UG",
					"CategoryTXT": "Urgent"
				}, {
					"Category": "SG",
					"CategoryTXT": "Suggestion"
				}, {
					"Category": "QS",
					"CategoryTXT": "Question"
				}, {
					"Category": "PB",
					"CategoryTXT": "Problem"
				}, {
					"Category": "ER",
					"CategoryTXT": "Error"
				}
			]
		});
		sap.ui.getCore().setModel(this.oCategoryModel, "CategoryModel");

		this.defaultMessageType = "S";
		this.selectedMessageType = this.defaultMessageType;

		// method called when view is ready to set the combo boxed with values from the model
		this.getView().addEventDelegate({
			onAfterShow: jQuery.proxy(function(evt) {
				this.onAfterShow(evt);
			}, this)
		});
	},

	onRouteMatched: function(evt) {
		var oParameters = evt.getParameters();
		if (oParameters.name !== "_ResponseView") {
			return;
		}

		this.vResponseEntryId = oParameters.arguments.entity;

		// set the Feed List with context messages
		this.oFeedList = this.getView().byId("feedListId");
		var filters = [new sap.ui.model.Filter("EntryId", sap.ui.model.FilterOperator.EQ, this.vResponseEntryId)];
		this.oFeedList.bindAggregation("items", {
			path: "/AppMessageFeedSet",
			template: sap.ui.xmlfragment("com.springer.fifsresponsefeed.view.FeedListItem", this),
			filters: filters
		});

		// do something after the items loaded
		var that = this;
		this.oFeedList.attachUpdateFinished(function() {
			that._onAfterFormRendering();
		});
	},

	_onAfterFormRendering: function() {
		/*
	1) the application allows to create a response for a message
	2) the service send multiple relevant context messages for the user
	3) the one message which is the reason for the response, has the attribute "Response" set to X
	4) we loop through the FeedListItems and assign a specialstyle class to this item to control the style via CSS
	5) in advanced we also save other context information because some are forwarded to the response attributes
*/
		var oFeedListItems = this.getView().byId("feedListId").getItems();
		for (var i = 0; i < oFeedListItems.length; i++) {
			var oItem = oFeedListItems[i];
			// the response attribute get a special class
			var vResponse = oItem.getBindingContext().getProperty("Response");
			if (vResponse === "X") {
				oItem.removeStyleClass("classNormalColor");
				oItem.addStyleClass("classRedColor");
				// information from the source message -> forwarded later to the response
				this.MSGsourceUser = oItem.getBindingContext().getProperty("UserId");
				this.MSGdepartment = oItem.getBindingContext().getProperty("Department");
				this.MSGcategory = oItem.getBindingContext().getProperty("Category");
				this.MSGOrigin = oItem.getBindingContext().getProperty("Origin");
			} else {
				oItem.removeStyleClass("classRedColor");
				oItem.addStyleClass("classNormalColor");
			}
		}
		this.getView().setBusy(false);
	},

	onAfterShow: function() {
		// set the message MessageType combo Box
		if (typeof this.oMessageTypeModel === "undefined") {
			this.oMessageTypeModel = sap.ui.getCore().getModel("MessageTypeModel");
		}

		var oComboBoxMessageType = this.getView().byId("MessageTypeSelector");
		oComboBoxMessageType.setModel(this.oMessageTypeModel);
		oComboBoxMessageType.bindAggregation("items", "/MessageTypeDetails",
			new sap.ui.core.Item({
				key: "{MessageType}",
				text: "{MessageTypeTXT}"
			}));
		oComboBoxMessageType.setSelectedKey(this.selectedMessageType);

		this.getView().byId("feedInputID").setIcon(
			com.springer.fifsresponsefeed.util.Formatter.getMessageTypeIcon(this.selectedMessageType));

	},

	changeComboboxValue: function(oEvent) {
		var selectedItem = oEvent.getParameter("selectedItem");
		if (selectedItem === null) {
			this.selectedMessageType = this.defaultMessageType;
			this.getView().byId("MessageTypeSelector").setSelectedKey(this.selectedMessageType);
		} else {
			this.selectedMessageType = selectedItem.getKey();
		}
		this.getView().byId("feedInputID").setIcon(
			com.springer.fifsresponsefeed.util.Formatter.getMessageTypeIcon(this.selectedMessageType));
	},

	onPost: function(oEvent) {
		this.getView().setBusy(true);
		var validKeys = this.oMessageTypeModel.getProperty("/MessageTypeDetails");
		for (var i = 0; i < validKeys.length; i++) {
			if (validKeys[i].MessageType === this.selectedMessageType) {
				var MessageTypeTXT = validKeys[i].MessageTypeTXT;
			}
		}
		console.log("Post MessageType:" + MessageTypeTXT);
		var feedbacktext = oEvent.getParameter("value");

		// save message
		var oEntry = {
			"EntryId": "EXTERN",
			"UserId": this.MSGsourceUser,
			"UserIdResponse": "EXTERN",
			"Responseentryid": this.vResponseEntryId,
			"Department": this.MSGdepartment,
			"Category": this.MSGcategory,
			"Origin": this.MSGOrigin,
			"MessageType": this.selectedMessageType,
			"CreatedTime": null,
			"Response": "X",
			"Message": feedbacktext
		};

		// Send OData Create request
		var that = this;
		var oModel = this.getView().getModel();
		oModel.create("/AppMessageFeedSet", oEntry, null,
			function(oData) {
				var vMessageUser = "";
				if (oData.Response === "X") {
					that.getView().byId("feedListId").setVisible(false);
					vMessageUser = "SUCCESS -> sent: " + feedbacktext;
					alert(vMessageUser);
					console.log(vMessageUser);
				} else {
					vMessageUser = "ERROR in BackEnd during save: " + oData.Message;
					alert(vMessageUser);
					console.log(vMessageUser);
				}
				that.getView().setBusy(false);
			}, function(oError) {
				that.getView().setBusy(false);
				console.log("Send failed " + oError);
			}
		);

	}

});