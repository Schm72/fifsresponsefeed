jQuery.sap.require("com.springer.fifsresponsefeed.util.Formatter");
jQuery.sap.require("com.springer.fifsresponsefeed.util.Controller");

com.springer.fifsresponsefeed.util.Controller.extend("com.springer.fifsresponsefeed.view.FirstPageNoID", {

	onInit: function() {
console.log("Init");
		this.getView().addEventDelegate({
			onAfterShow: jQuery.proxy(function(evt) {
				this.onAfterShow(evt);
			}, this)
		});
		this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);
	},

	onRouteMatched: function(evt) {
		var oParameters = evt.getParameters();
		// check that we are in the URL hash pattern http://domain:port/%PATTERN%
		if (oParameters.name !== "_FirstPageNoID") {
			return;
		}
	},
	
	onAfterShow: function() {
		var textArea = this.getView().byId("ContextText");
		textArea.setVisible(true);
		textArea.setValue("There is no Message ID");
	}

});