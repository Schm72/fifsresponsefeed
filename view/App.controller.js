jQuery.sap.require("com.springer.fifsresponsefeed.util.Formatter");
jQuery.sap.require("com.springer.fifsresponsefeed.util.Controller");

com.springer.fifsresponsefeed.util.Controller.extend("com.springer.fifsresponsefeed.view.App", {
	/**
	 * initializing controller, subscribe two "OfflineStore" channel event
	 */
	onInit: function() {
	},

	/**
	 * UI5 OfflineStore channel Refreshing event handler, refreshing the offline store data
	 * @param{String} sChannel event channel name
	 * @param{String}} sEvent event name
	 * @param{Object}} oData event data object
	 */
	onRefreshing: function(sChannel, sEvent, oData) {
		this.getView().getModel().refresh();
	},

	/**
	 * UI5 OfflineStore channel Synced event handler, after refreshing offline store, refresh data model
	 * @param{String} sChannel event channel name
	 * @param{String}} sEvent event name
	 * @param{Object}} oData event data object
	 */
	onDataReady: function(sChannel, sEvent, oData) {
		console.log("refreshed oData Model with offline store");
		this.getView().setBusy(false);
		this.getView().getModel().refresh();
	}
});